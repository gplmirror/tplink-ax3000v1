(function($){

	$.su = $.su || {};
	$.su.CHAR = {
       PROTAL :{ 
           SUCCESS:"Login com Sucesso!",
           FAILED:"Login falhou!",
           WELCOME:"Bem-Vindo",
           PASSWORD:"Senha",
           LOGIN:"Entrar",
           ACCEPT_TERMS_OF_USE:"Aceito os <a class=\"link\" href=\"#\" target=\"_blank\">Termos de Utilização</a>"
},
       ERROR :{ 
           "00000001":"Esta campo é necessário.",
           "00000002":"Formato inválido."
		}
	};

})(jQuery);

					
