(function($){

	$.su = $.su || {};
	$.su.CHAR = {
       PROTAL :{ 
           SUCCESS:"ログインに成功しました！",
           FAILED:"ログインに失敗しました！",
           WELCOME:"こんにちは！",
           PASSWORD:"パスワード",
           LOGIN:"ログイン",
           ACCEPT_TERMS_OF_USE:"私は利用規約 <a class=\"link\" href=\"#\" target=\"_blank\">を承諾します。</a>"
},
       ERROR :{ 
           "00000001":"この項目は必須です。",
           "00000002":"無効な形式です。"
		}
	};

})(jQuery);

					
