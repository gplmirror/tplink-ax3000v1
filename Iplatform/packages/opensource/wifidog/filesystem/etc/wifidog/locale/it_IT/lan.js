(function($){

	$.su = $.su || {};
	$.su.CHAR = {
       PROTAL :{ 
           SUCCESS:"Login avvenuto con successo!",
           FAILED:"Login Fallito!",
           WELCOME:"Benvenuti",
           PASSWORD:"Password",
           LOGIN:"Login",
           ACCEPT_TERMS_OF_USE:"Accetto i <a class=\"link\" href=\"#\" target=\"_blank\">Termini d'Uso</a>"
},
       ERROR :{ 
           "00000001":"Questo campo è obbligatorio.",
           "00000002":"Formato non valido."
		}
	};

})(jQuery);

					
