(function($){

	$.su = $.su || {};
	$.su.CHAR = {
       PROTAL :{ 
           SUCCESS:"Автентифікація успішна!",
           FAILED:"Помилка автентифікації!",
           WELCOME:"Вітаємо",
           PASSWORD:"Пароль",
           LOGIN:"Авторизація",
           ACCEPT_TERMS_OF_USE:"Я приймаю <a class=\"link\" href=\"#\" target=\"_blank\">Умови використання</a>"
},
       ERROR :{ 
           "00000001":"Це поле є обов'язковим.",
           "00000002":"Невірний формат."
		}
	};

})(jQuery);

					
