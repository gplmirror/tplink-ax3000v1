(function($){

	$.su = $.su || {};
	$.su.CHAR = {
       PROTAL :{ 
           SUCCESS:"Login succesvol!",
           FAILED:"Login mislukt!",
           WELCOME:"Welkom",
           PASSWORD:"Wachtwoord",
           LOGIN:"Login",
           ACCEPT_TERMS_OF_USE:"Ik accepteer de <a class=\"link\" href=\"#\" target=\"_blank\">Gebruikersvoorwaarden </a>"
},
       ERROR :{ 
           "00000001":"Dit veld is verplicht.",
           "00000002":"Ongeldig formaat."
		}
	};

})(jQuery);

					
