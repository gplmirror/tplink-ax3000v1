(function($){

	$.su = $.su || {};
	$.su.CHAR = {
       PROTAL :{ 
           SUCCESS:"Zalogowano!",
           FAILED:"Nie udało się zalogować!",
           WELCOME:"Witaj",
           PASSWORD:"Hasło",
           LOGIN:"Zaloguj",
           ACCEPT_TERMS_OF_USE:"Wyrażam zgodę na <a class=\"link\" href=\"#\" target=\"_blank\">Warunki użytkowania</a>"
},
       ERROR :{ 
           "00000001":"Pole wymagane.",
           "00000002":"Niewłaściwy format."
		}
	};

})(jQuery);

					
