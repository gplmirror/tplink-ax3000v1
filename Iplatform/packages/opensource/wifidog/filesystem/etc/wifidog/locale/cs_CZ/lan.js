(function($){

	$.su = $.su || {};
	$.su.CHAR = {
       PROTAL :{ 
           SUCCESS:"Úspěšné přihlášení!",
           FAILED:"Přihlášení selhalo!",
           WELCOME:"Vítejte",
           PASSWORD:"Heslo",
           LOGIN:"Přihlásit",
           ACCEPT_TERMS_OF_USE:"Souhlasím s <a class=\"link\" href=\"#\" target=\"_blank\">Podmínkami použití</a>"
},
       ERROR :{ 
           "00000001":"Tuto položku je nutné vyplnit.",
           "00000002":"Neplatný formát."
		}
	};

})(jQuery);

					
