(function($){

	$.su = $.su || {};
	$.su.CHAR = {
       PROTAL :{ 
           SUCCESS:"Prihlásenie úspešné!",
           FAILED:"Prihlásenie zlyhalo!",
           WELCOME:"Vitajte",
           PASSWORD:"Heslo",
           LOGIN:"Prihlásiť sa",
           ACCEPT_TERMS_OF_USE:"Súhlasím s <a class=\"link\" href=\"#\" target=\"_blank\">Podmienkami používania</a>"
},
       ERROR :{ 
           "00000001":"Toto pole sa vyžaduje.",
           "00000002":"Neplatný formát."
		}
	};

})(jQuery);

					
