(function($){

	$.su = $.su || {};
	$.su.CHAR = {
       PROTAL :{ 
           SUCCESS:"로그인 성공!",
           FAILED:"로그인 실패!",
           WELCOME:"환영합니다",
           PASSWORD:"비밀번호",
           LOGIN:"로그인",
           ACCEPT_TERMS_OF_USE:"I accept the <a class=\"link\" href=\"#\" target=\"_blank\">Terms of Use</a>"
},
       ERROR :{ 
           "00000001":"필수 입력값입니다. ",
           "00000002":"잘못된 형식입니다."
		}
	};

})(jQuery);

					
