(function($){

	$.su = $.su || {};
	$.su.CHAR = {
       PROTAL :{ 
           SUCCESS:"Влизането е успешно!",
           FAILED:"Влизането е неуспешно!",
           WELCOME:"Добре дошли",
           PASSWORD:"Парола",
           LOGIN:"Вход",
           ACCEPT_TERMS_OF_USE:"Приемам <a class=\"link\" href=\"#\" target=\"_blank\">Условията за използване</a>"
},
       ERROR :{ 
           "00000001":"Това поле е задължително.",
           "00000002":"Невалиден формат."
		}
	};

})(jQuery);

					
