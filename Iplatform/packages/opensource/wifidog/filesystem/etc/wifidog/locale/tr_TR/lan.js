(function($){

	$.su = $.su || {};
	$.su.CHAR = {
       PROTAL :{ 
           SUCCESS:"Giriş Başarılı!",
           FAILED:"Giriş Başarısız!",
           WELCOME:"Hoşgeldiniz",
           PASSWORD:"Parola",
           LOGIN:"Oturum Aç",
           ACCEPT_TERMS_OF_USE:"<a class=\"link\" href=\"#\" target=\"_blank\">Kullanım Koşulları</a>nı kabul ediyorum."
},
       ERROR :{ 
           "00000001":"Bu alan zorunludur.",
           "00000002":"Geçersiz format."
		}
	};

})(jQuery);

					
