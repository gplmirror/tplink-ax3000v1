(function($){

	$.su = $.su || {};
	$.su.CHAR = {
      PROTAL :{ 
           SUCCESS:"¡Acceso Correcto!",
           FAILED:"¡Acceso Fallido!",
           WELCOME:"Bienvenido",
           PASSWORD:"Contraseña",
           LOGIN:"Acceder",
           ACCEPT_TERMS_OF_USE:"Accepto los  <a class=\"link\" href=\"#\" target=\"_blank\"> Términos de Uso</a>"
},
       ERROR :{ 
           "00000001":"Se requiere este campo.",
           "00000002":"Formato no válido."
		}
	};

})(jQuery);

					
