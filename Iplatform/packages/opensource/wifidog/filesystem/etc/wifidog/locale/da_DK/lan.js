(function($){

	$.su = $.su || {};
	$.su.CHAR = {
       PROTAL :{ 
           SUCCESS:"Login lykkedes!",
           FAILED:"Login mislykkedes!",
           WELCOME:"Velkommen",
           PASSWORD:"Adgangskode",
           LOGIN:"Login",
           ACCEPT_TERMS_OF_USE:"Jeg accepterer <a class=\"link\" href=\"#\" target=\"_blank\">Vilkår for brug</a>"
},
       ERROR :{ 
           "00000001":"Dette felt er obligatorisk.",
           "00000002":"Ugyldigt format."
		}
	};

})(jQuery);

					
