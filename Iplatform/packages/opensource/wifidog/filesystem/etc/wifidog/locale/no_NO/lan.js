(function($){

	$.su = $.su || {};
	$.su.CHAR = {
       PROTAL :{ 
           SUCCESS:"Innlogging lyktes!",
           FAILED:"Innlogging mislyktes",
           WELCOME:"Velkommen",
           PASSWORD:"Passord",
           LOGIN:"Innlogging",
           ACCEPT_TERMS_OF_USE:"Jeg aksepterer <a class=\"link\" href=\"#\" target=\"_blank\">Vilkår og betingelser</a>"
},
       ERROR :{ 
           "00000001":"Dette feltet er obligatorisk.",
           "00000002":"Ugyldig format."
		}
	};

})(jQuery);

					
