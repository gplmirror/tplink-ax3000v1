(function($){

	$.su = $.su || {};
	$.su.CHAR = {
       PROTAL :{ 
           SUCCESS:"Вход выполнен успешно!",
           FAILED:"Ошибка при входе!",
           WELCOME:"Добро пожаловать",
           PASSWORD:"Пароль",
           LOGIN:"Вход",
           ACCEPT_TERMS_OF_USE:"Я соглашаюсь с <a class=\"link\" href=\"#\" target=\"_blank\">Правилами использования</a>"
},
       ERROR :{ 
           "00000001":"Необходимо данное поле. ",
           "00000002":"Неправильный формат."
		}
	};

})(jQuery);

					
