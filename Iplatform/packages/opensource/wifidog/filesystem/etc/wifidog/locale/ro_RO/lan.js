(function($){

	$.su = $.su || {};
  $.su.CHAR = {
       PROTAL :{ 
           SUCCESS:"Autentificare cu Succes!",
           FAILED:"Autentificare Eșuată!",
           WELCOME:"Bine ați venit",
           PASSWORD:"Parolă",
           LOGIN:"Autentificare",
           ACCEPT_TERMS_OF_USE:"Accept  <a class=\"link\" href=\"#\" target=\"_blank\">Termenii de Utilizare</a>"
},
       ERROR :{ 
           "00000001":"Acest câmp este necesar.",
           "00000002":"Format incorect."
		}
	};

})(jQuery);

					
