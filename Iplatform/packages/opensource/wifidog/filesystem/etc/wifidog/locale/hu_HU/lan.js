(function($){

	$.su = $.su || {};
	$.su.CHAR = {
		PROTAL: {
			SUCCESS:"Login Successfully!",
			FAILED:"Bejelentkezési hiba.",
			WELCOME: "Üdvözöljük",
			PASSWORD: "Jelszó",
			LOGIN: "Belépés",
			ACCEPT_TERMS_OF_USE:"Elfogadom a <a class=\"link\" href=\"#\" target=\"_blank\"> Felhasználási feltételeket </a>"
		},
		ERROR: {
			"00000001":"A mező kitöltése kötelező.",
			"00000002":"Érvénytelen formátum."
		}
	};

})(jQuery);

					
