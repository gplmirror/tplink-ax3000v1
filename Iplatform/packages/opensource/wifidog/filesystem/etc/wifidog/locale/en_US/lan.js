(function($){

	$.su = $.su || {};
	$.su.CHAR = {
		PROTAL: {
			SUCCESS:"Login Successfully!",
			FAILED:"Login Failed!",
			WELCOME:"Welcome",
			PASSWORD:"Password",
			LOGIN:"Login",
			ACCEPT_TERMS_OF_USE: "I accept the <a class=\"link\" href=\"#\" target=\"_blank\">Terms of Use</a>"
		},
		ERROR: {
			"00000001":"This field is required.",
			"00000002":"Invalid format."
		}
	};

})(jQuery);

					
