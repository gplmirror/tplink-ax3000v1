(function($){

	$.su = $.su || {};
	$.su.CHAR = {
       PROTAL :{ 
           SUCCESS:"Einloggen erfolgreich",
           FAILED:"Einloggen fehlgeschlagen!",
           WELCOME:"Willkommen",
           PASSWORD:"Passwort",
           LOGIN:"Einloggen",
           ACCEPT_TERMS_OF_USE:"Ich akzeptiere die <a class=\"link\" href=\"#\" target=\"_blank\">Nutzungsbedingungen</a>"
},
       ERROR :{ 
           "00000001":"Bitte füllen Sie dieses Feld aus.",
           "00000002":"Ungültiges Format."
		}
	};

})(jQuery);

					
