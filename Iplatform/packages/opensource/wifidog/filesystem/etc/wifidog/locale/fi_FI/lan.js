(function($){

	$.su = $.su || {};
	$.su.CHAR = {
        PROTAL: {
           SUCCESS:"Kirjautuminen onnistui!",
           FAILED:"Kirjautuminen epäonnistui!",
           WELCOME:"Tervetuloa",
           PASSWORD:"Salasana",
           LOGIN:"Kirjaudu sisään",
           ACCEPT_TERMS_OF_USE:"Hyväksyn <a class=\"link\" href=\"#\" target=\"_blank\">käyttöehdot</a>"
},
       ERROR :{ 
           "00000001":"Tämä kenttä on pakollinen.",
           "00000002":"Virheellinen muoto."
		}
	};

})(jQuery);

					
