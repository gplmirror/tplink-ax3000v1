(function($){

	$.su = $.su || {};
	$.su.CHAR = {
       PROTAL :{ 
           SUCCESS:"登入成功！",
           FAILED:"登入失敗！",
           WELCOME:"歡迎",
           PASSWORD:"密碼",
           LOGIN:"登入",
           ACCEPT_TERMS_OF_USE:"我接受<a class=\"link\" href=\"#\" target=\"_blank\">使用條款</a>"
},
       ERROR :{ 
           "00000001":"此欄位為必填欄位。",
           "00000002":"錯誤的格式。"
		}
	};

})(jQuery);

					
