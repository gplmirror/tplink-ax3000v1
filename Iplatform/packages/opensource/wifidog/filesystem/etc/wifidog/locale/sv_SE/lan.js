(function($){

	$.su = $.su || {};
	$.su.CHAR = {
       PROTAL :{ 
           SUCCESS:"Lyckad inloggning!",
           FAILED:"Inloggning misslyckades!",
           WELCOME:"Välkommen",
           PASSWORD:"Lösenord",
           LOGIN:"Logga in",
           ACCEPT_TERMS_OF_USE:"Jag accepterar <a class=\"link\" href=\"#\" target=\"_blank\"> användarvillkoren </a>"
},
       ERROR :{ 
           "00000001":"Det här fältet är obligatoriskt.",
           "00000002":"Ogiltigt format."
		}
	};

})(jQuery);

					
