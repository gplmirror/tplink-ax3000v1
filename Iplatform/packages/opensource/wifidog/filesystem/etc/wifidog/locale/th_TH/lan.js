(function($){

	$.su = $.su || {};
	$.su.CHAR = {
       PROTAL :{ 
           SUCCESS:"เข้าสู่ระบบสำเร็จ",
           FAILED:"Login Failed!",
           WELCOME:"ยินดีต้อนรับ",
           PASSWORD:"รหัสผ่าน",
           LOGIN:"เข้าสู่ระบบ",
           ACCEPT_TERMS_OF_USE:"I accept the <a class=\"link\" href=\"#\" target=\"_blank\">Terms of Use</a>"
},
       ERROR :{ 
           "00000001":"ช่องนี้จำเป็นต้องกรอก",
           "00000002":"รูปแบบผิด"
		}
	};

})(jQuery);

					
