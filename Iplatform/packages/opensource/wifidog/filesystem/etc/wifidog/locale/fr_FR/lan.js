(function($){

	$.su = $.su || {};
	$.su.CHAR = {
       PROTAL :{ 
           SUCCESS:"Succès de l'authentification !",
           FAILED:"Échec de l'authentification !",
           WELCOME:"Bienvenue",
           PASSWORD:"Mot de passe",
           LOGIN:"Connexion",
           ACCEPT_TERMS_OF_USE:"J'accepte les  <a class=\"link\" href=\"#\" target=\"_blank\">Conditions d'utilisation<a/>"
},
       ERROR :{ 
           "00000001":"Ce champ est requis.",
           "00000002":"Format incorrect."
		}
	};

})(jQuery);

					
