#
# Basic targets for special product
# Writen by zhuyu@18.01.25 for Intel GRX350 AX50 Projecct.
#

SOFTWARE_VERSION?="V1.0.4P1"

	
sdk_post:
	[ ! -d "$(SDK_SOURCE_DIR)/modules" ] || rm -rf $(SDK_SOURCE_DIR)/modules
	mkdir -p $(SDK_SOURCE_DIR)/modules
	for node in `find $(SDK_SOURCE_DIR)/ -name "*.ko" -type f`; do \
		cp -f $$node $(SDK_SOURCE_DIR)/modules; \
	done

sdk.config:
	@cp -f $(CONFIG_DIR)/sdk.config $(SDK_BUILD_DIR)/.config


kernel.config:
	echo "[TPIPF] copy kernel.config"
	@cp -f $(CONFIG_DIR)/kernel.config $(SDK_SOURCE_DIR)/.config


sdk: world sdk_post
sdk_menuconfig: menuconfig
sdk_package/symlinks: package/symlinks

sdk_build:
	@if [ -e $(SDK_ROOT_DIR)/.config ]; then \
		$(SUBMAKE) -C $(SDK_ROOT_DIR) $(SDK_FLAGS); \
	fi
	@$(SUBMAKE) sdk_post
	
sdk_clean:
	@if [ -e $(SDK_ROOT_DIR)/.config ]; then \
		$(SUBMAKE) -C $(SDK_ROOT_DIR) clean $(SDK_FLAGS); \
	fi
