TPLINK GPL code for Archer AX3000(US) 1.0

Build Guide:
1. cd ./Iplatform/build
2. make

Notice:
1. When you are tring to build GPL code, the make program will automatic download
   some other source code packages that it needs from Internet, please make sure 
   your Linux PC have good Internet connection.

2. If you are of the opinion that TP-LINK should offer further source code subject
   to the GPL, please contact us under 'support@tp-link.com'.
